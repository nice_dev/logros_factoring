import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { HistorialPage } from '../historial/historial';
import { OperacionesService } from '../../services/operaciones.service';
import { Operaciones } from '../../models/operaciones.model';
import { LocalStorageService } from '../../services/localstorage.service';
import { StorageService } from '../../services/storage.service';

/**
 * Generated class for the OperacionesPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-operaciones',
  templateUrl: 'operaciones.html',
})
export class OperacionesPage {

  operaciones: Operaciones = new Operaciones()
  constructor(private _operacionesService: OperacionesService, private _localStorageService: LocalStorageService, private _storageService: StorageService, public navCtrl: NavController, public navParams: NavParams) {
  }

  getOperaciones() {
    this._operacionesService.obtenerOperaciones(this._storageService.getCurrentToken(), this._localStorageService.cliente.rut).subscribe((data: Operaciones) => {
      this.operaciones = data

    })

  }
  ionViewDidEnter() {
    this.getOperaciones()
  }
  abrirHistorial() {
    this.navCtrl.push(HistorialPage)
  }
}
