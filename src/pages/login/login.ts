import { Component } from '@angular/core';
import { NavController, LoadingController, Loading, Platform } from 'ionic-angular';
import { Validators, FormGroup, FormControl } from '@angular/forms';
import { InicioPage } from '../inicio/inicio';
import { InAppBrowser, InAppBrowserOptions } from '@ionic-native/in-app-browser';
import { LoginService } from '../../services/login.service';
import { StorageService } from '../../services/storage.service';
import {App} from 'ionic-angular';
import { ToastController } from 'ionic-angular'
import { LocalStorageService } from '../../services/localstorage.service';
import 'rxjs/add/operator/timeout';
import { Device } from '@ionic-native/device';

@Component({
  selector: 'login-page',
  templateUrl: 'login.html'
})
export class LoginPage {
  login: FormGroup;
  main_page: { component: any };
  loading: Loading;
  errorRut: string;
  modelo:any;
  loadingvideo: boolean;
  mainvideo :HTMLVideoElement;
  constructor(private device : Device,private platform :  Platform ,private _localStorageService: LocalStorageService, public app: App, private _storageService: StorageService,  private _loginService: LoginService, public nav: NavController, public loadingCtrl: LoadingController, private inAppBrowser: InAppBrowser, public toastCtrl:ToastController) {
    this.loadingvideo = true;
    this.main_page = { component: InicioPage };
    this.login = new FormGroup({
      rut: new FormControl(' ', [Validators.required]),
      password: new FormControl(' ', [Validators.required])
    });
  platform.ready().then(()=>{
    this.modelo =this.device.model;
   

  })
    
  }
  

  doLogin() {
    this._loginService.generateToken("Us3RMoV1l","MoV1L-1993").timeout(3000).subscribe((data)=>{
      var token = JSON.parse(data).token;
      var rut = this.login.value['rut'];
      this._storageService.setCurrentSession({"token": token})
      rut = rut.toUpperCase()
      rut = rut.replace(/[^0-9K]+/g, '');
      this._loginService.login(rut, this.login.value['password'], token).subscribe(
        (data: any) => {
        console.log(data);
        if(data['cliente']!=null){
          this._localStorageService.setCliente(data['cliente'])
          this._localStorageService.setEjecutivo(data['ejecutivo'])
          this.app.getActiveNav().setRoot(InicioPage);
        }
      
      },
      (error)=>{console.log(error)},
      ()=>{})
      
    })

  }

  cleanToken() {
    this._storageService.logout();
    
    
  }


  openWebPage() {
    const options: InAppBrowserOptions = {
      clearcache: 'yes',
      toolbar: 'yes',
      closebuttoncaption: 'Cerrar',
      zoom:'no',
      hideurlbar: 'yes',
      navigationbuttoncolor:'#c01d04',
      closebuttoncolor: '#c01d04',
      allowinlinemediaplayback:'yes'
    }
    const browser = this.inAppBrowser.create('https://www.empresaslogros.cl/productos-de-factoring-consulta-gratuita?', '_blank', options);
  
    browser.show();
  }


   onChangeRut() {
    this.errorRut = null;
    var rut = this.login.controls.rut.value;
    if (rut.length > 3) {
      rut = rut.toUpperCase()
      rut = rut.replace(/[^0-9K]+/g, '');
      var dv = rut.slice(-1).toUpperCase();
      rut = rut.slice(0, -1);

      var sum = 0;
      var mult = 2;
      var i: number;
      for (i = 1; i <= rut.length; i++) {
        var index = mult * rut.charAt(rut.length - i);
        sum += index;
        if (mult < 7) { mult = mult + 1; } else { mult = 2; }
      }
      var dvCalculated: any = 11 - (sum % 11);
      dvCalculated = (dvCalculated == 10) ? 'K' : dvCalculated;
      dvCalculated = (dvCalculated == 11) ? 0 : dvCalculated;

      // Validar que el Cuerpo coincide con su Dígito Verificador
      if (dvCalculated != dv) {
        this.errorRut = "El rut ingresado no es valido"
      }
      var formatedRut: String = '-' + dv;
      var asc: number = 1;
      for (i = rut.length - 1; i >= 0; i--) {
        formatedRut = rut.charAt(i) + formatedRut;
        if (asc % 3 == 0 && i > 0)
          formatedRut = '.' + formatedRut;
        asc++;
      }
      this.login.controls.rut.setValue(formatedRut);
    }
  }

  
  ionViewWillEnter(){
    this.cleanToken();
    this.mainvideo  = <HTMLVideoElement> document.getElementById('video');
    
  }


  forgotPassword() {
    
    const options: InAppBrowserOptions = {
      clearcache: 'yes',
      toolbar: 'yes',
      closebuttoncaption: 'Cerrar',
      zoom:'no',
      hideurlbar: 'yes',
      navigationbuttoncolor:'#c01d04',
      closebuttoncolor: '#c01d04',
      allowinlinemediaplayback:'yes'
    }

    const browser = this.inAppBrowser.create('https://logros.cl/site_new/recuperar_clave.php', '_blank', options);
    //browser.on('exit').subscribe(()=>{location.reload()})
    browser.show();

  }
  

}


