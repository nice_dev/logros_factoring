import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams,LoadingController} from 'ionic-angular';
import { LocalStorageService } from '../../services/localstorage.service';
import { StorageService } from '../../services/storage.service';
import { Retenciones } from '../../models/retenciones.model';
import { RetencionesService } from '../../services/retenciones.service';

/**
 * Generated class for the RetencionesPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-retenciones',
  templateUrl: 'retenciones.html',
})
export class RetencionesPage {
 retenciones: Retenciones[] = []
 datos  = true; 

  constructor(public loadingCtrl : LoadingController,private _localStorageService : LocalStorageService, private _storageService: StorageService, private _retencionesService : RetencionesService,public navCtrl: NavController, public navParams: NavParams) {
  }

  getRetenciones(){
    this._retencionesService.obtenerRetenciones(this._storageService.getCurrentToken(),this._localStorageService.cliente.rut).subscribe((data:Retenciones[])=>{
      this.retenciones = data
      console.log(data)
      if(data.length ==0){
        this.datos = false
        console.log(this.datos)
        
      }else{
       
      }
    })
  }

  ionViewDidLoad(){
    this.getRetenciones()
    
  }
  presentLoading() {
    
    const loader = this.loadingCtrl.create({
      content: "Por favor espere ...",
      duration: 3000
    });
    loader.present();
  }
}


