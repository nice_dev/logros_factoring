import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, LoadingController } from 'ionic-angular';
import { OperacionesCursoService } from '../../services/operaciones_curso.service';
import { LocalStorageService } from '../../services/localstorage.service';
import { StorageService } from '../../services/storage.service';
import { OperacionCurso } from '../../models/operacionesCurso.model'
import { OperacionesVigenteService } from '../../services/operaciones_vigentes.service';
import { OperacionesVigentes } from '../../models/operacionesVigentes.model';


/**
 * Generated class for the HistorialPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-historial',
  templateUrl: 'historial.html',
})
export class HistorialPage {
  historial: string = "Vigentes"
  operacionesCurso: OperacionCurso[] = []
  operacionesVigente: OperacionesVigentes[] = []
  total: number = 0
  op_curso = true;
  op_vigente = true;
  op_agrupadas: any = {};
  op_agrupadas_curso : any = {};
  op_agrupadas_curso_keys : any = [];
  op_agrupadas_keys = []
  constructor(public nav : NavController,public _loadingCtrl: LoadingController, private _opVigente: OperacionesVigenteService, private _opCurso: OperacionesCursoService, private _localStorageService: LocalStorageService, private _storageService: StorageService, public navCtrl: NavController, public navParams: NavParams) {

  }

  getOperacionesCurso() {
    this._opCurso.obtenerOperacionesCurso(this._storageService.getCurrentToken(), this._localStorageService.cliente.rut).subscribe((data: any) => {
      this.op_agrupadas_curso_keys = [];
      this.total = data['total']
      if(this.total ==0){
        this.op_curso = false
      }
      for (let index in data['operacion']) {
        let op_actual = data['operacion'][index];
        op_actual['monto'] = new Intl.NumberFormat('de-DE').format(op_actual['monto']);
        // este es un string que representa fecha
        let fecha = op_actual['fechaotorgamiento'];
        // se convierte a date
        var parts = fecha.split("/");
        fecha = new Date(+parts[2], parts[1] - 1, +parts[0]);
        
        if (!(fecha in this.op_agrupadas_curso)){
          this.op_agrupadas_curso[fecha] = [];
          this.op_agrupadas_curso_keys.push(fecha);
        }
        this.op_agrupadas_curso[fecha].push(op_actual);
       
        }
        this.op_agrupadas_curso_keys.sort(function(a, b){return b.getTime() - a.getTime()});
    
      })
  }

  getOperacionesVigente() {
    this._opVigente.obtenerOperacionesVigente(this._storageService.getCurrentToken(), this._localStorageService.cliente.rut).subscribe((data: any) => {
      this.op_agrupadas_keys = [];
      this.total = data['total']
      if(this.total == 0 ){
        this.op_vigente = false
      }
      for (let index in data['operacion']) {
        let op_actual = data['operacion'][index];
        op_actual['saldo'] = new Intl.NumberFormat('de-DE').format(op_actual['saldo']);
        // este es un string que representa fecha
        let fecha = op_actual['fechaotorgamiento'];

        // se convierte a date
        var parts = fecha.split("/");
        fecha = new Date(+parts[2], parts[1] - 1, +parts[0]);
        
        if (!(fecha in this.op_agrupadas)){
          this.op_agrupadas[fecha] = [];
          this.op_agrupadas_keys.push(fecha);
        }
        this.op_agrupadas[fecha].push(op_actual);

      }
      this.op_agrupadas_keys.sort(function(a, b){return b.getTime() - a.getTime()});
      console.log("operacioens vigentes", this.op_agrupadas)
    })
  }

  volver(){
    this.nav.pop()
  }

  ionViewDidLoad() {
    this.getOperacionesVigente();
    this.getOperacionesCurso();
    
  }
  
 
}
   


