import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import * as L from 'leaflet';
import { EjecutivoModel } from '../../models/ejecutivo.model';
import { LocalStorageService } from '../../services/localstorage.service';
import { SucursalModel } from '../../models/sucursa.model';

/**
 * Generated class for the MapPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-map',
  templateUrl: 'map.html',
})
export class MapPage {
  
  map: any;
  ejecutivo: EjecutivoModel = new EjecutivoModel()
  sucursal: SucursalModel = new SucursalModel()
  constructor(private _localStorageService: LocalStorageService, public navCtrl: NavController, public navParams: NavParams) {
    this.ejecutivo = this._localStorageService.getEjecutivo()
    this.sucursal = this.ejecutivo.sucursal
    console.log(this.sucursal)
  }

  abrirMapa() {
    let map = L.map('map').setView([this.ejecutivo.sucursal.latitud, this.ejecutivo.sucursal.longitud], 16);
    L.tileLayer('http://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
      attribution: 'Logros',
      errorTileUrl: '/assets/images/no-internet.png',
      maxZoom: 18
    }).addTo(map);
    L.marker([this.ejecutivo.sucursal.latitud, this.ejecutivo.sucursal.longitud]).addTo(map);
  }

  getEjecutivo() {
    this._localStorageService.getEjecutivo()
  }

  volver(){
    this.navCtrl.pop();
  }

  ionViewDidLoad() {
    this.abrirMapa();
  }
  
  ionViewCanLeave(){
    document.getElementById("map").outerHTML = "";
  }

}
