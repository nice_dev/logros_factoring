import { Component } from '@angular/core';
import { LocalStorageService } from '../../services/localstorage.service';
import { StorageService } from '../../services/storage.service';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { IndicadoresService } from '../../services/indicadores.service';
import { Indicadores } from '../../models/indicador.model';
/**
 * Generated class for the IndicadoresPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-indicadores',
  templateUrl: 'indicadores.html',
})
export class IndicadoresPage {
  
  indicador :  Indicadores [] = []

  
  constructor(private _indicadoresService : IndicadoresService,private _localStorageService : LocalStorageService, private _storageService : StorageService,public navCtrl: NavController, public navParams: NavParams) {
    
    

  }

  getIndicadores(){
    this._indicadoresService.obtenerIndicadores(this._storageService.getCurrentToken(),this._localStorageService.cliente.rut).subscribe((data:Indicadores[])=>{
      console.log("esta data viene del componente")
      this.indicador = data
      console.log(data)
    })
      
    
  }

  ionViewDidLoad(){
    this.getIndicadores()
    //console.log(this._localStorageService.getSaldos());
  }
  
  }

