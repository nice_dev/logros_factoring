import { Component } from '@angular/core';
import { trigger, state, style, animate, transition } from '@angular/animations';


@Component({
  selector: 'page-loading',
  templateUrl: 'loading.html',
  animations:[
    trigger('fadeInOut', [
      state('void', style({
        opacity: 0
      })),
      transition('void => fade', animate(1000)),
    ])
  ]
})
export class LoadingPage {

  constructor() {
  }

  ionViewDidLoad() {
  }

}
