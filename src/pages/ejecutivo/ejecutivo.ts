import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, Nav } from 'ionic-angular';
import { LocalStorageService } from '../../services/localstorage.service';
import { EjecutivoModel } from '../../models/ejecutivo.model';
import { SucursalModel } from '../../models/sucursa.model';
import { Cliente } from '../../models/cliente.model';
import { StorageService } from '../../services/storage.service';
import { LoginPage } from '../login/login';
import { InAppBrowser, InAppBrowserOptions } from '@ionic-native/in-app-browser';
import { CallNumber } from '@ionic-native/call-number';
import { MapPage } from '../map/map';

@IonicPage()
@Component({
  selector: 'page-ejecutivo',
  templateUrl: 'ejecutivo.html',
})

export class EjecutivoPage {
  // @ViewChild('map') mapContainer: ElementRef;
  // map: any;
  ejecutivo: EjecutivoModel = new EjecutivoModel()
  cliente: Cliente = new Cliente()
  sucursal: SucursalModel = new SucursalModel()
  src  :any;
  constructor(private callNumber: CallNumber, private iab: InAppBrowser,
    private nav: Nav, private _storageService: StorageService,
    public navCtrl: NavController, public navParams: NavParams,
    private _localStorageService: LocalStorageService) {
    this.ejecutivo = this._localStorageService.getEjecutivo()
    this.cliente = this._localStorageService.getCliente()
    this.sucursal = this.ejecutivo.sucursal

  }

  abrirMapa() {
    this.nav.push(MapPage);
  }

  getCliente() {
    this._localStorageService.getCliente()
  }

  getEjecutivo() {
    this._localStorageService.getEjecutivo()
  }


  volver() {
    this.navCtrl.pop()
  }

  openDialer() {
    this.callNumber.callNumber(this.ejecutivo['celular'], true)
      .then(res => console.log('Launched dialer!', res))
      .catch(err => console.log('Error launching dialer', err));
  }

  openWeb() {
    const options: InAppBrowserOptions = {
      clearcache: 'yes',
      toolbar: 'yes',
      closebuttoncaption: 'Cerrar',
      zoom:'no',
      hideurlbar: 'yes',
      navigationbuttoncolor:'#c01d04',
      closebuttoncolor: '#c01d04'
    }

    const browser = this.iab.create('https://www.empresaslogros.cl/productos-de-factoring-consulta-gratuita?', '_blank', options);
    browser.show();
  }
  
  logout() {
    this._storageService.logout()
    this.nav.setRoot(LoginPage);
  }


}
