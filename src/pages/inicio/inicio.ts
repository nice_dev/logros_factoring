import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { trigger, state, style, animate, transition} from '@angular/animations';
import { LocalStorageService } from '../../services/localstorage.service';
import { StorageService } from '../../services/storage.service';
import { Saldo } from '../../models/saldos.model';
import { SaldosService } from '../../services/saldos.service';
import { IndicadoresService } from '../../services/indicadores.service';
import { Indicadores } from '../../models/indicador.model';
import { EjecutivoPage } from '../ejecutivo/ejecutivo';
import { HistorialPage } from '../historial/historial';
import { OperacionesVigentes } from '../../models/operacionesVigentes.model';
import { OperacionesVigenteService } from '../../services/operaciones_vigentes.service';
import { OperacionesCursoService } from '../../services/operaciones_curso.service';
import { OperacionCurso } from '../../models/operacionesCurso.model';
import { Cliente } from '../../models/cliente.model';



@IonicPage()
@Component({
  selector: 'page-inicio',
  templateUrl: 'inicio.html',
  animations:[
    trigger('fadeInOut', [
      state('void', style({
        opacity: 0
      })),
      transition('void => fade', animate(1000)),
    ])
  ]
})
export class InicioPage {
  
  cliente: Cliente = new Cliente()
  loading : boolean = true;
  loadingService: boolean = false;
  today: number = Date.now();
  saldos: any[] = [];
  total: any = 0;
  indicador :  Indicadores [] = [];
  opVigentes: OperacionesVigentes [] = [];
  opCurso : OperacionCurso [] =[];
  contador = 0;
  
  TotalOperaciones : number = 0;
  transformado_dolar:any;
  transformado_euro:any;
  transformado_uf:any;
  constructor(private _operacionesCurso : OperacionesCursoService,private _operacionesVigService : OperacionesVigenteService,private _indicadoresService : IndicadoresService,private _storageService:StorageService, private _saldoService: SaldosService, public navCtrl: NavController, public navParams: NavParams, private _localStorageService : LocalStorageService) {
    this.cliente = this._localStorageService.getCliente()
  }
  getSaldos(){
    this.loadingService = false
    this._saldoService.obtenerSaldos(this._storageService.getCurrentToken(),this._localStorageService.cliente.rut).subscribe((data:Saldo)=>{
      this.saldos = [];
      this.loadingService = true;
      for (let key in data){
        console.log(data[key])
        if (data[key] > 0 && key != 'total'){
          this.saldos.push({name: key,
                            saldo: new Intl.NumberFormat('de-DE').format(data[key])
                          })
        }
      }
      this.total = new Intl.NumberFormat('de-DE').format(data["total"]);
    })
  }


  getIndicadores(){
    this.loadingService = false
    
    this._indicadoresService.obtenerIndicadores(this._storageService.getCurrentToken(),this._localStorageService.cliente.rut).subscribe((data:Indicadores[])=>{
      let dolar :any;
    let euro :any;
    let uf : any;
      console.log("esta data viene del componente")
      this.indicador = data;
      console.log (data);
      this.loadingService = true
      for(let item of this.indicador){
        dolar = item.dolar;
        euro= item.euro;
        uf=item.uf;
        this.transformado_dolar = new Intl.NumberFormat('de-DE').format(dolar);
        this.transformado_euro = new Intl.NumberFormat('de-DE').format(euro);
        this.transformado_uf = new Intl.NumberFormat('de-DE').format(uf);


      }
    })
      
    
  }

  getOperacionesVigentes(){
    this.loadingService = false
    this._operacionesVigService.obtenerOperacionesVigente(this._storageService.getCurrentToken(),this._localStorageService.cliente.rut).subscribe((data:OperacionesVigentes[])=>{
      console.log("esta data viene del componente")
      this.opVigentes = data
      this.loadingService = true
      console.log(data)
      
      
    })
      
  }

  getOperacionesCurso(){
    this.loadingService = false
    this._operacionesCurso.obtenerOperacionesCurso(this._storageService.getCurrentToken(), this._localStorageService.cliente.rut).subscribe((data:OperacionCurso[])=>{
      this.opCurso = data
      this.contador +=1;
      this.loadingService = true
      console.log("operaciones en curso ", data)
      
    })
  }
  
  perfil(){
    this.navCtrl.push(EjecutivoPage);
  }
  
  operaciones(){
    this.navCtrl.push(HistorialPage);
  }


  
  ionViewDidLoad(){
  
  this.getIndicadores()
  this.getSaldos();
  this.getOperacionesVigentes()
  this.getOperacionesCurso()
  setTimeout(()=>{if(this.loading){this.loading=false}}, 3000);

}


   
}
