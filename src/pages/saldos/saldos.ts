import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { LocalStorageService } from '../../services/localstorage.service';
import { StorageService } from '../../services/storage.service';
import { Saldo } from '../../models/saldos.model';
import { SaldosService } from '../../services/saldos.service';

/**
 * Generated class for the SaldosPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-saldos',
  templateUrl: 'saldos.html',
})
export class SaldosPage {
  saldos: Saldo= new Saldo()
  
  constructor(private _storageService:StorageService, private _saldoService: SaldosService, public navCtrl: NavController, public navParams: NavParams, private _localStorageService : LocalStorageService) {
    //this.saldos = this._localStorageService.getSaldos()
    
  }
  
getSaldos(){
  this._saldoService.obtenerSaldos(this._storageService.getCurrentToken(),this._localStorageService.cliente.rut).subscribe((data:Saldo)=>{
    this.saldos = data
    console.log(data)
  })
}

ionViewDidEnter(){
  this.getSaldos()

  //console.log(this._localStorageService.getSaldos());
}

  }

  


