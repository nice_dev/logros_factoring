export class SucursalModel {
    fono: string;
    dir : string;
    mail : string;
    latitud : number;
    longitud: number;
    nombre_sucursal : string;
}