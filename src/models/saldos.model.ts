export class Saldo{
     "Factoring Tradicional": number;
     "Factoring Financiero": number;
     "Financiamiento Ordenes de Compra": number;
     "Financiamiento de Contratos": number;
     "Financiamiento Bono de Riego": number;
     "Financiamiento Subsidio Serviu": number;
     "Factoring Internacional": number;
     "Confirming": number;
     "Financiamiento Compra de Inventario": number;
     "Crédito Capital de Trabajo": number;
     "total": number;
     "Fuera de Plazo": number;
}