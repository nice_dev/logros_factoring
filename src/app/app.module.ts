import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { CommonModule } from '@angular/common';
import { IonicApp, IonicModule } from 'ionic-angular';
import { MyApp } from './app.component';
import { TranslateModule, TranslateLoader } from '@ngx-translate/core';
import { TranslateHttpLoader } from '@ngx-translate/http-loader';
import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { HttpModule } from '@angular/http';
import { HttpClient, HttpClientModule } from '@angular/common/http';
import { registerLocaleData } from '@angular/common';
import { Device } from '@ionic-native/device';
//pages

import { SaldosPage } from '../pages/saldos/saldos';
import { OperacionesPage } from '../pages/operaciones/operaciones';
import { RetencionesPage } from '../pages/retenciones/retenciones';
import { IndicadoresPage } from '../pages/indicadores/indicadores';
import { EjecutivoPage } from '../pages/ejecutivo/ejecutivo';
import { InicioPage } from '../pages/inicio/inicio';
import { HistorialPage} from '../pages/historial/historial';
import { LoginPage } from '../pages/login/login';
import { MapPage } from '../pages/map/map';


//custom components
import { PreloadImage } from '../components/preload-image/preload-image';
import { BackgroundImage } from '../components/background-image/background-image';
import { ShowHideContainer } from '../components/show-hide-password/show-hide-container';
import { ShowHideInput } from '../components/show-hide-password/show-hide-input';
import { ColorRadio } from '../components/color-radio/color-radio';
import { CounterInput } from '../components/counter-input/counter-input';
import { Rating } from '../components/rating/rating';
import { VideoPlayerModule } from '../components/video-player/video-player.module';
import { ValidatorsModule } from '../components/validators/validators.module';
import localeEs from '@angular/common/locales/es-CL';
import { Ng2Rut } from 'ng2-rut';
//services
import { LanguageService } from '../providers/language/language.service';
import { StorageService } from '../services/storage.service';
import { LoginService } from '../services/login.service';
import { TokenService } from '../services/token.service';
import { SaldosService } from '../services/saldos.service';
import { ClienteService } from '../services/cliente.service';
import { LocalStorageService } from '../services/localstorage.service';
import { RetencionesService } from '../services/retenciones.service';
import { IndicadoresService } from '../services/indicadores.service';
import { OperacionesService } from '../services/operaciones.service';
import { OperacionesCursoService } from '../services/operaciones_curso.service';
import { OperacionesVigenteService } from '../services/operaciones_vigentes.service';

// Ionic Native Plugins
import { SplashScreen } from '@ionic-native/splash-screen';
import { StatusBar } from '@ionic-native/status-bar';
import { SocialSharing } from '@ionic-native/social-sharing';
import { NativeStorage } from '@ionic-native/native-storage';
import { InAppBrowser } from '@ionic-native/in-app-browser';
import { Keyboard } from '@ionic-native/keyboard';
import { Geolocation } from '@ionic-native/geolocation';
import { AppRate } from '@ionic-native/app-rate';
import { ImagePicker } from '@ionic-native/image-picker';
import { Crop } from '@ionic-native/crop';
import { EmailComposer } from '@ionic-native/email-composer';
import { CurrencyPipe } from '@angular/common';
import { AppVersion } from '@ionic-native/app-version/ngx';
import { CallNumber } from '@ionic-native/call-number';
import { LoadingPage } from '../pages/loading/loading';

export function createTranslateLoader(http: HttpClient) {
  return new TranslateHttpLoader(http, './assets/i18n/', '.json');
}

registerLocaleData(localeEs, 'es-CL');

@NgModule({
  declarations: [
    MyApp,
    SaldosPage,
    OperacionesPage,
    RetencionesPage,
    IndicadoresPage,
    EjecutivoPage,
    InicioPage,
    HistorialPage,
    LoginPage,
    MapPage,
    LoadingPage,
    //custom components
    PreloadImage,
    BackgroundImage,
    ShowHideContainer,
    ShowHideInput,
    ColorRadio,
    CounterInput,
    Rating,
    
  ],
  imports: [
    BrowserModule,
    //NoopAnimationsModule,
    BrowserAnimationsModule,
    HttpModule,
    HttpClientModule,
    CommonModule,
    Ng2Rut,
    IonicModule.forRoot(MyApp, {
			modalEnter: 'modal-slide-in',
			modalLeave: 'modal-slide-out',
			pageTransition: 'ios-transition',
			swipeBackEnabled: false
		}),
		TranslateModule.forRoot({
    loader: {
				provide: TranslateLoader,
				useFactory: (createTranslateLoader),
				deps: [HttpClient]
			}
		}),
		VideoPlayerModule,
		ValidatorsModule
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    SaldosPage,
    OperacionesPage,
    RetencionesPage,
    IndicadoresPage,
    EjecutivoPage,
    InicioPage,
    HistorialPage,
    LoginPage,
    MapPage,
    LoadingPage,

  ],
  providers: [
    //functionalities
    LanguageService,
    Device,
    //ionic native plugins
	  SplashScreen,
	  StatusBar,
    SocialSharing,
    NativeStorage,
    CallNumber,
    InAppBrowser,
    Keyboard,
    Geolocation,
		AppRate,
		ImagePicker,
		Crop,
    EmailComposer,
    CurrencyPipe,
    AppVersion,
    LoginService,
    StorageService,
    TokenService,
    SaldosService,
    ClienteService,
    LocalStorageService,
    RetencionesService,
    IndicadoresService,
    OperacionesService,
    OperacionesCursoService,
    OperacionesVigenteService,
  ],
  schemas: [ CUSTOM_ELEMENTS_SCHEMA ]
})
export class AppModule {}
