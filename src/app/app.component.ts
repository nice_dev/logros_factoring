import { Component, ViewChild } from '@angular/core';
import { Platform, MenuController, Nav, App, ToastController } from 'ionic-angular';
import { SplashScreen } from '@ionic-native/splash-screen';
import { StatusBar } from '@ionic-native/status-bar';

import { TranslateService } from '@ngx-translate/core';
import { LoginPage } from '../pages/login/login';
import { SaldosPage } from '../pages/saldos/saldos';
import { OperacionesPage } from '../pages/operaciones/operaciones';
import { RetencionesPage } from '../pages/retenciones/retenciones';
import { EjecutivoPage } from '../pages/ejecutivo/ejecutivo';
import { IndicadoresPage } from '../pages/indicadores/indicadores';

import { InicioPage } from '../pages/inicio/inicio';
import { timer} from 'rxjs/observable/timer';
import { StorageService } from '../services/storage.service';

@Component({
  selector: 'app-root',
  templateUrl: 'app.html'
})
export class MyApp {

  @ViewChild(Nav) nav: Nav;

  // make WalkthroughPage the root (or first) page
  //rootPage: any = WalkthroughPage;
  
  // aqui pon el componente que quieres cargar al inicio
  rootPage: any = LoginPage;

  pages: Array<{title: any, icon: string, component: any}>;
  pushPages: Array<{title: any, icon: string, component: any}>;
  showSplash = true;
  constructor(
    platform: Platform,
    public menu: MenuController,
    public app: App,
    public splashScreen: SplashScreen,
    public statusBar: StatusBar,
    public translate: TranslateService,
    public toastCtrl: ToastController,
    private _storageService: StorageService
  ) {

    platform.ready().then(() => {
        platform.registerBackButtonAction(() => {
          this.nav.pop();
        });
   
      // Okay, so the platform is ready and our plugins are available.
      // Here you can do any higher level native things you might need.
      this.splashScreen.hide();
      this.statusBar.styleDefault();
      timer(3000).subscribe(()=>this.showSplash = false)
    });
    this.pages = [
      { title: "Inicio", icon: 'home', component: InicioPage },
      { title: "Saldos", icon: 'logo-usd', component: SaldosPage },
      { title: "Operaciones", icon: 'swap', component: OperacionesPage },
      { title: "Retenciones", icon: 'alert', component: RetencionesPage },
      { title: "Indicadores", icon: 'trending-up', component: IndicadoresPage },
      { title: "Ejecutivo", icon: 'person', component: EjecutivoPage }
    ];

  }

  
  openPage(page) {
    // close the menu when clicking a link from the menu
    this.menu.close();
    // navigate to the new page if it is not the current page
    this.nav.push(page.component);
  }

  pushPage(page) {
    // close the menu when clicking a link from the menu
    this.menu.close();
    // rootNav is now deprecated (since beta 11) (https://forum.ionicframework.com/t/cant-access-rootnav-after-upgrade-to-beta-11/59889)
    this.nav.setRoot(page.component);
  }

  logout(){
    this._storageService.logout()
    this.nav.setRoot(LoginPage);
  }
}
