import { Injectable } from "@angular/core";
import { Http } from '@angular/http';
import 'rxjs/add/operator/toPromise';
import { NativeStorage } from '@ionic-native/native-storage';
import { LoadingController, AlertController } from "ionic-angular";
@Injectable()
export class ComunicationService {
    constructor(public http: Http, public nativeStorage: NativeStorage,
        public loadingCtrl: LoadingController, public alertCtrl: AlertController) {
        
    }
}
