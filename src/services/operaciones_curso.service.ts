import { Injectable } from "@angular/core";
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { tap, catchError } from 'rxjs/operators';
import { App, LoadingController, AlertController } from 'ionic-angular';


@Injectable()
export class OperacionesCursoService {
  api_url = "https://www.logros.cl/site/servicios/celular/jwt/index.php?p=";
  constructor(private http: HttpClient, public app: App, public loadingCtrl: LoadingController, public alertCtrl : AlertController) { }

  obtenerOperacionesCurso(token:string,rut:string): any {
    
    const headers = new HttpHeaders({
      'Access-Control-Allow-Origin': '*',
      'Content-Type': 'application/json',
      'Access-Control-Allow-Methods': 'POST, GET, OPTIONS, PUT',
      'Accept': 'application/json',
      'Authorization': 'Bearer ' + token,
    })
    //headers.append('Authorization', 'Bearer ' + this.token)
    const options = {
      headers: headers
    }
    return this.http.post(this.api_url + "op_curso", { "rut":rut}, options).pipe(
      tap(async (res) => {
        
      }),
      catchError(e => {
        
          const alert = this.alertCtrl.create({
            title: 'Error',
            subTitle: 'No se pudieron obtener los datos',
            buttons: ['OK']
          })
          alert.present()
        console.log(e)
        throw new Error(e);
      })
    )
  }

}