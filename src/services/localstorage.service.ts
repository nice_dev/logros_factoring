import { Injectable } from "@angular/core";
import { Cliente } from '../models/cliente.model';
import { EjecutivoModel } from '../models/ejecutivo.model';


@Injectable()
export class LocalStorageService {
    cliente: Cliente = new Cliente()
    ejecutivo: EjecutivoModel = new EjecutivoModel()
    //token = "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJleHAiOjE1NjcxNzY2MTUsImRhdGEiOnsiaWQiOjEsIm5hbWUiOiJVczNSTW9WMWwifX0.CjxnsSoTvagGpvDSThkOAfPPMlG_xuuteNAldAurQnw";
    constructor( ) { }

    getCliente() {
        return this.cliente
    }

    setCliente(cliente: Cliente) {
        this.cliente = cliente
    }

    getEjecutivo() {
        return this.ejecutivo
    }

    setEjecutivo(ejecutivo: EjecutivoModel) {
        this.ejecutivo = ejecutivo
    }

}