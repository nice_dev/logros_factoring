import { Injectable } from "@angular/core";
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { tap, catchError } from 'rxjs/operators';
import { App, LoadingController, AlertController } from 'ionic-angular';


@Injectable()
export class ClienteService {
  api_url = "https://www.logros.cl/site/servicios/celular/jwt/index.php?p=";
  constructor(private http: HttpClient, public app: App, public loadingCtrl: LoadingController, public alertCtrl : AlertController) { }

  GetEjecutivo (token:string,rut:string): any{
    let loading = this.loadingCtrl.create({
      content: 'Cargando ...'
    });
    loading.present();
    const headers = new HttpHeaders({
      'Access-Control-Allow-Origin': '*',
      'Content-Type': 'application/json',
      'Access-Control-Allow-Methods': 'POST, GET, OPTIONS, PUT',
      'Accept': 'application/json',
      'Authorization': 'Bearer ' + token,
    })
    headers.append('Authorization', 'Bearer ' + token)
    console.log(headers)
    const options = {
      headers: headers
    }
    
    return this.http.post(this.api_url + "login", { }, options).pipe(

      tap(async (res) => {
        loading.dismiss();
      }),
      catchError(e => {
        loading.dismiss();
        
          const alert = this.alertCtrl.create({
            title: 'Error',
            subTitle: 'No se pudieron obtener los datos',
            buttons: ['OK']
          })
          alert.present()
        console.log(e)
        throw new Error(e);
      })
    )

  }

  
}