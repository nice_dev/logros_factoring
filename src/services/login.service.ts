import { Injectable } from "@angular/core";
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { tap, catchError } from 'rxjs/operators';
import { App, LoadingController } from 'ionic-angular';
import { AlertController } from 'ionic-angular';
import { map } from 'rxjs/operators';


@Injectable()
export class LoginService {
  api_url = "https://www.logros.cl/site/servicios/celular/jwt/index.php?p=";
  username = "Us3RMoV1l";
  password = "MoV1L-1993";
  constructor(private http: HttpClient, public app: App, public alertCtrl: AlertController, public loadingCtrl: LoadingController) { }

  generateToken(nombreusuario:string, password:string){
    let loading = this.loadingCtrl.create({
      spinner:'hide',
      content: 'Autentificando ...',
      cssClass: 'my-loading-class'
    });
    loading.present();
    const options = {
      headers: {"Authorization": 'Basic ' + btoa(nombreusuario + ':' + password),
                "Access-Control-Allow-Origin": "*"}
    }

    return this.http.get("https://www.logros.cl/site/servicios/celular/jwt/index.php?p=autentica", options).pipe(map(res=>{
      loading.dismiss();
      return JSON.stringify(res);
    },
    catchError(e => {
      loading.dismiss();
      if(e.status == 404 || e.status ==400){
        const alert = this.alertCtrl.create({
          title: 'Error',
          subTitle: 'No se pudieron obtener los datos',
          buttons: ['OK']
        })
        alert.present}
        else if(e.status == 403){
          const alert = this.alertCtrl.create({
            title:'Error',
            subTitle:'Usuario y/o contraseña incorrectas',
            buttons: ['Ok']
          })
        
        alert.present();
      }
      
      throw new Error(e);
    })
    ))
  }

  login(rut:string,password:string,token:string): any {
    

    const headers = new HttpHeaders({
      'Access-Control-Allow-Origin': '*',
      'Content-Type': 'application/json',
      'Access-Control-Allow-Methods': 'POST, GET, OPTIONS, PUT',
      'Accept': 'application/json',
      'Authorization': 'Bearer ' + token
    })
    
    const options = {
      headers: headers
    }
   
    return this.http.post(this.api_url + "login", { 'rut': rut, 'clave': password }, options).pipe(
      tap(async (res) => {
      }),
      catchError(e => {
        
        if(e.status == 404 || e.status ==400){
          const alert = this.alertCtrl.create({
            title: 'Error',
            subTitle: 'No se pudieron obtener los datos',
            buttons: ['OK']
          })
          alert.present}
          else if(e.status == 403){
            const alert = this.alertCtrl.create({
              title:'Error',
              subTitle:'Usuario y/o contraseña incorrectas',
              buttons: ['Ok']
            })
          
          alert.present();
        }
        
        throw new Error(e);
      })
    )
  }
  
}