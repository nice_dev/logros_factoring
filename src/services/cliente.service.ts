import { Injectable } from "@angular/core";
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { tap, catchError } from 'rxjs/operators';
import { App } from 'ionic-angular';


@Injectable()
export class ClienteService {
  api_url = "https://www.logros.cl/site/servicios/celular/jwt/index.php?p=";
  token = "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJleHAiOjE1NjY1NzY1NjksImF1ZCI6IjMxOGY1MDVlMzViNWVkYTkwODFmYzVmMGZkMTNlM2RhYTY0ZDA4ZWUiLCJkYXRhIjp7ImlkIjoxLCJuYW1lIjoiVXMzUk1vVjFsIn19.Pv0nIBbWPSjG88OUEjKS7le7RDyc4E3zwxwLlZxaTRo";
  constructor(private http: HttpClient, public app: App) { }

  GetCliente (): any{

    const headers = new HttpHeaders({
      'Access-Control-Allow-Origin': '*',
      'Content-Type': 'application/json',
      'Access-Control-Allow-Methods': 'POST, GET, OPTIONS, PUT',
      'Accept': 'application/json',
      'Authorization': 'Bearer ' + this.token,
    })
    headers.append('Authorization', 'Bearer ' + this.token)
    console.log(headers)
    const options = {
      headers: headers
    }
    
    return this.http.post(this.api_url + "login", { }, options).pipe(

      tap(async (res) => {
      
      }),
      catchError(e => {
        console.log(e)
        throw new Error(e);
      })
    )

  }

  
}