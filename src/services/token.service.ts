import { Injectable } from "@angular/core";
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { tap, catchError } from 'rxjs/operators';
import { App } from 'ionic-angular';

@Injectable()
export class TokenService {
  api_url = "https://www.logros.cl/site/servicios/celular/jwt/index.php?p=";
  token : any;
  constructor(private http: HttpClient, public app: App) { }

  generarToken(username:string, password:string) {
    const headers = new HttpHeaders()
    headers.append("Access-Control-Allow-Origin","*")
    headers.append("Authorization","Basic "+btoa("Us3RMoV1l:MoV1L-1993"))
    const options = {
      headers: headers
    }
    console.log("jojo generador de token")
    return this.http.post(this.api_url + "autentica", {'usuario': username, 'clave':password}, options).pipe(
      tap(async (res) => {
       console.log(res);
       this.token(res);
      }),
      catchError(e => {
        console.log("error")
        console.log(e)
        throw new Error(e);
      })
    )
  }
}